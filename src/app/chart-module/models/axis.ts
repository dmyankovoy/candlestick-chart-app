import { Chart } from './chart';
import { AxisLabel, AxisXLabel, AxisYLabel, ChartElement, ChartOptions, Point, CandleData } from './';

export abstract class Axis implements ChartElement {
    constructor() {}

    labels: AxisLabel[];

    abstract getAxisPoint(options): Point;
    abstract show(chart: Chart): boolean;

    renderLabels(chart: Chart): void {
        this.labels.forEach(l => l.render(chart));
    };
    
    render(chart: Chart): void {
        if(!this.show(chart)) {
            return;
        }

        const axisEndPoint = this.getAxisPoint(chart);
        chart.painter.line({ x: 0, y: 0 }, axisEndPoint, '#cccccc', 0.5);

        this.renderLabels(chart);
    }
}

export class XAxis extends Axis {
    constructor(labels: Date[]) {
        super();
        this.labels = labels.map((l, idx) => new AxisXLabel(idx, l));
    }

    show(chart: Chart) {
        return chart.options.showXAxis;
    }

    getAxisPoint(chart: Chart): Point {
        return {
            x: chart.painter.viewport.width,
            y: 0
        };
    }
}

export class YAxis extends Axis {    
    constructor(labels: number[]) {
        super();       
        this.labels = labels.map((l, idx) => new AxisYLabel(l, l));
    }

    show(chart: Chart) {
        return chart.options.showYAxis;
    }

    getAxisPoint(chart: Chart): Point {
        return {
            x: 0,
            y: chart.painter.viewport.height
        };
    }
}
