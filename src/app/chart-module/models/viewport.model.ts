export interface ViewPort {
    width: number;
    height: number;    
}