import { Chart } from './chart';
import { ChartElement, ChartOptions, Point } from './';

export abstract class AxisLabel implements ChartElement {
    protected color: string = '#3494e2';

    constructor(protected idx: number, protected value: any) {
    }

    abstract getText(chart: Chart, value: any): string;

    abstract getPoint(chart: Chart, idx: number) : Point;

    abstract renderSplitLine(chart: Chart, coord: Point);
    
    abstract render(chart: Chart): void;

    protected isFunc = (func) => {
        return func && {}.toString.call(func) === '[object Function]';
    }
}

export class AxisXLabel extends AxisLabel {
    getPoint(chart: Chart, idx: number): Point {
        const barPlace = chart.painter.viewport.width / chart.chartViewportState.barCount;
        const x = barPlace * (idx + 1) - barPlace / 2;
        return { x: x, y: -15 };
    }

    renderSplitLine(chart: Chart, startPoint: Point): void {
        startPoint.y = -3;
        const endPoint: Point = { x: startPoint.x, y: chart.painter.viewport.height };
        chart.painter.line(startPoint, endPoint, this.color, 1, 0.3);
    }

    getText(chart: Chart, value: Date): string {
        if(this.isFunc(chart.options.axisXLabelFormatter)) {
            return chart.options.axisXLabelFormatter(value);
        }
        return value.toString();
    }

    render(chart: Chart): void {
        if((chart.chartViewportState.barCount - (this.idx + 1) ) % chart.options.axisXLabelsEvery === 0)
         {
            if(this.idx <= 0) {
                return;
            }

            const coord = this.getPoint(chart, this.idx);
            chart.painter.text(coord, this.getText(chart, this.value), this.color, 'center');
            this.renderSplitLine(chart, coord);
        }
    }
}

export class AxisYLabel extends AxisLabel {
    getPoint(chart: Chart, val: number): Point {        
        return { x: -10, y: chart.chartViewportState.getY(this.idx) };
    }

    getText(chart: Chart, value: number): string {
        if(this.isFunc(chart.options.axisXLabelFormatter)) {
            return chart.options.axisYLabelFormatter(value);
        }
        return value.toString();
    }

    renderSplitLine(chart: Chart, startPoint: Point): void {
        startPoint.x = -3;
        const endPoint: Point = { x: chart.painter.viewport.width, y: startPoint.y };
        chart.painter.line(startPoint, endPoint, this.color, 1, 0.3);
    }

    render(chart: Chart): void {        
        const coord = this.getPoint(chart, this.idx);
        this.renderSplitLine(chart, coord);
        coord.y = coord.y - 3;
        chart.painter.text(coord, this.getText(chart, this.value), this.color, 'right');
    }
}