import { Point, ViewPort } from './';

export class Painter {
    private ctx: any;
    private _viewport: ViewPort;

    // Начало координат графика
    private origin: Point;

    private width;
    private height;

    private paddings: number[];

    get viewport(): ViewPort {
        return this._viewport;
    };
    
    constructor(private ref: any) {
        this.ctx = this.ref.getContext("2d");        
        this.ctx.font = "14px Arial";        
    }    

    clear(): void {
        this.ctx.clearRect(0, 0, this.width, this.height);
    }

    setViewport(width: number, height: number, paddings: number[]): void {
        this.width = width;
        this.height = height;

        this.paddings = paddings;

        this.ref.setAttribute('width', width);
        this.ref.setAttribute('height', height);        

        let top: number, right: number, bottom: number, left: number;
        [top, right, bottom, left] = paddings;

        this.origin = {
            x: left,
            y: height - bottom - top
        };

        this._viewport = {
            width: width -  right - left,
            height: height - bottom - top            
        }
    }    

    line(start: Point, end: Point, color: string, lineWidth: number, alpha: number = 1): void {
        this.ctx.beginPath();        

        const translatedStart = this.translatePoint(start);
        const translatedEnd = this.translatePoint(end);

        this.ctx.moveTo(translatedStart.x, translatedStart.y);
        this.ctx.lineTo(translatedEnd.x, translatedEnd.y);
        
        this.ctx.globalAlpha = alpha;
        this.ctx.strokeStyle = color;
        this.ctx.lineWidth = lineWidth;       

        this.ctx.stroke();
        this.ctx.globalAlpha = 1;
    }

    rectangle(color: string, x: number, y: number, width: number, height: number, alpha: number = 1): void {
        this.ctx.fillStyle = color;
        this.ctx.globalAlpha = alpha;

        const t = this.translatePoint({ x: x, y: y + height });

        this.ctx.fillRect(t.x, t.y, width, height);
    }

    text(point: Point, text: string, color: string, align: string = 'left'): void {
        const t = this.translatePoint(point);
        this.ctx.textAlign = align;
        this.ctx.fillStyle = color;
        this.ctx.fillText(text, t.x, t.y);
        this.ctx.globalAlpha = 1;
    }
    
    translatePoint(point: Point): Point {
        return {
            x: this.origin.x + point.x,
            y: this.origin.y - point.y + this.paddings[0]
        };
    }
}