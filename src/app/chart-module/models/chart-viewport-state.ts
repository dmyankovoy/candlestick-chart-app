import { Chart } from './chart';
import { Painter } from './';

export class ChartViewportState {
    private ratio: number;
    private volumeRatio: number;
    private barPlace: number;
    private defaultPadding: number = 1;

    public get barCount(): number {
        return this._barCount;
    }

    private get valuePadding(): number {
        return this.chart.options.showVolumes ? 50 : 0;
    };

    constructor(private chart: Chart, private _barCount: number, private min: number, private max: number, private maxVolume) {        
        this.resize();
    }

    getCandleRect(idx: number, low: number, hight: number, barWidth: number, padding: number = this.defaultPadding): number[] {
        if(barWidth === null) {
            barWidth = this.barPlace - padding * 2;
        } else {
            padding = (this.barPlace - barWidth) / 2;
        }

        const x = idx * this.barPlace + padding;
        const yLow = (low - this.min) / this.ratio;
        const yHight = (hight - this.min) / this.ratio;
        const height = yHight - yLow;

        return [x, yLow + this.valuePadding, barWidth, height];
    }

    getVolumeRect(idx: number, volume: number, barWidth: number, padding: number = this.defaultPadding): number[] {
        if(barWidth === null) {
            barWidth = this.barPlace - padding * 2;
        } else {
            padding = this.barPlace - barWidth / 2;
        }

        const x = idx * this.barPlace + 1;
        const y = 0;
        const width = barWidth;
        const height = volume / this.volumeRatio;

        return [x, y, barWidth, height];
    }

    getY(value: number) {
        return (value - this.min) / this.ratio;
    }

    resize() {
        this.ratio = (this.max - this.min) / (this.chart.painter.viewport.height - this.valuePadding);
        this.volumeRatio = this.maxVolume / this.valuePadding;
        this.barPlace = this.chart.painter.viewport.width / this._barCount;
    }
}