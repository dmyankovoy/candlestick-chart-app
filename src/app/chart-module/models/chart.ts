import { XAxis, YAxis } from './axis';
import { Candle, CandleData, ChartElement, ChartOptions, ChartViewportState, Painter, Point } from './';

export class Chart {
    private _painter: Painter;    
    public get painter() : Painter {
        return this._painter;
    }

    private _chartViewportState: ChartViewportState;
    public get chartViewportState() : ChartViewportState {
        return this._chartViewportState;
    }
    
    private axis: ChartElement[] = [];
    private elements: ChartElement[] = [];

    public get options(): ChartOptions {
        return this._options;
    }

    constructor(private ref, private _options: ChartOptions) {        
        this._painter = new Painter(this.ref);
        this._options = _options;
        this.resize();    
    }    
    
    setData(data: any): void {
        const dataModel = this.getDataModel(data, this._options.maxCandleCount);

        if(dataModel.length === 0) {
            return;
        }

        this.elements = dataModel.map((x, idx) => new Candle(x, idx));        

        const filtered = dataModel.filter(x => x.min != undefined);

        const min =  Math.min(...filtered.map(x => x.min));
        const max =  Math.max(...filtered.map(x => x.max));
        const maxVolume = Math.max(...filtered.map(x => x.volume));        

        const xlabels = dataModel.map(x => x.time);
        const yLabelsInterval = (max - min) / this._options.axisYLabels;
        let yLabel = min;
        const yLabels: number[] = [];
        while(yLabel <= max) {
            yLabels.push(yLabel);
            yLabel += yLabelsInterval;
        }

        this.axis.push(new XAxis(xlabels));
        this.axis.push(new YAxis(yLabels));

        this._chartViewportState = new ChartViewportState(this, dataModel.length, min, max, maxVolume);
    }

    setOptions(options: ChartOptions) {        
        this._options = options;        
        this.render();
    }

    resize(): void {
        let width = this.ref.parentElement.offsetWidth;
        if(width > this.options.maxWidth) {
            width = this.options.maxWidth;
        }

        const paddings = [this._options.paddingTop, this._options.paddingRight, this._options.paddingBottom, this._options.paddingLeft];

        this.painter.setViewport(width, this.options.height, paddings);

        if(this._chartViewportState) {
            this._chartViewportState.resize();
        }

        if(width === 0) {
            setTimeout(() => {
                this.resize();
            }, 0);
            return;
        }

        this.render();
    }

    render(): void {
        this._painter.clear();
        this.renderElements(this.axis);
        this.renderElements(this.elements);
    }

    private getDataModel(data, take): CandleData[] {        
        const timeIntervals: CandleData[] = [];
        let i = data.t.length > take ? data.t.length - take : 0;       

        for (i; i < data.t.length; i++) {            
            timeIntervals.push(new CandleData(new Date(data.t[i] * 1000), data.l[i], data.h[i], data.o[i], data.c[i], data.v[i]));
        }

        return timeIntervals;
    }

    private renderElements = (elements: ChartElement[]): void => {
        if(elements.length === 0) {
            return;
        }

        elements.forEach(el => el.render(this));
    }
}