export class CandleData {
    get isRise(): boolean {
        return this.close > this.open;
    }

    get top(): number {
        return this.isRise ? this.close : this.open;
    }

    get bottom(): number {
        return this.isRise ? this.open : this.close;
    }

    constructor(public time: Date, 
                public min: number,
                public max: number, 
                public open: number, 
                public close: number, 
                public volume: number) {
    }
}