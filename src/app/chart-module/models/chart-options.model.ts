export class ChartOptions {
    
    private _maxCandleCount = 100;

    public get maxCandleCount(): number {
        return this._maxCandleCount;   
    };
    public set maxCandleCount(v: number) {
        if(v > 0) {
            this._maxCandleCount = v;
        }
    };
    
    private _maxWidth: number = 800;
    public get maxWidth() : number {
        return this._maxWidth;
    }
    public set maxWidth(v: number) {
        if(v > 0) {
            this._maxWidth = v;
        }       
    }

    private _height: number = 400;
    public get height() : number {
        return this._height;
    }
    public set height(v: number) {
        if(v > 0) {
            this._height = v;
        }       
    }

    private _paddingLeft: number = 20;
    public get paddingLeft() : number {
        return this._paddingLeft;
    }
    public set paddingLeft(v: number) {
        if(v > 0 && v < this._maxWidth) {
            this._paddingLeft = v;
        }       
    }

    private _paddingRight: number = 20;
    public get paddingRight() : number {
        return this._paddingRight;
    }
    public set paddingRight(v: number) {
        if(v > 0 && v < this._maxWidth) {
            this._paddingRight = v;
        }       
    }

    private _paddingTop: number = 20;
    public get paddingTop() : number {
        return this._paddingTop;
    }
    public set paddingTop(v: number) {
        if(v > 0 && v < this._height) {
            this._paddingTop = v;
        }       
    }
    
    private _paddingBottom: number = 20;
    public get paddingBottom() : number {
        return this._paddingBottom;
    }
    public set paddingBottom(v: number) {
        if(v > 0 && v < this._height) {
            this._paddingBottom = v;
        }       
    }
      
    public showVolumes: boolean = true;

    public showXAxis: boolean = true;

    private _axisXLabelsEvery: number = 10;
    public get axisXLabelsEvery() : number {
        return this._axisXLabelsEvery;
    }
    public set axisXLabelsEvery(v: number) {
        if(v > 0) {
            this._axisXLabelsEvery = v;
        }       
    }
    
    public axisXLabelFormatter = (val: Date): string => { return (<Date>val).toISOString().substr(11, 5) };

    public showYAxis: boolean = false;    

    private _axisYLabels: number = 5;
    public get axisYLabels() : number {
        return this._axisYLabels;
    }
    public set axisYLabels(v: number) {
        if(v > 0) {
            this._axisYLabels = v;
        }       
    }

    public axisYLabelFormatter = (val: number): string => { return val.toString() };
}