import { Chart } from './chart';
import { ChartOptions, Point } from './';

export interface ChartElement {
    render(chart: Chart): void;
}