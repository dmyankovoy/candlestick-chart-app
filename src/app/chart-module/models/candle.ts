import { Chart } from './chart';
import { CandleData, ChartElement, ChartOptions, Point } from './';

export class Candle implements ChartElement {

    private color: string;
    private shadowColor = '#666666';

    constructor(private candleData: CandleData, private idx: number) {        
        this.color = candleData.isRise ? '#00e100' : '#ff0000';
    }    
    
    render(chart: Chart): void {
        const shadow = chart.chartViewportState.getCandleRect(this.idx, this.candleData.min, this.candleData.max, 1);
        chart.painter.rectangle(this.shadowColor, shadow[0], shadow[1], shadow[2], shadow[3]);        

        const bar = chart.chartViewportState.getCandleRect(this.idx, this.candleData.bottom, this.candleData.top, null);
        chart.painter.rectangle(this.color, bar[0], bar[1], bar[2], bar[3]);

        if(chart.options.showVolumes) {
            const volume = chart.chartViewportState.getVolumeRect(this.idx, this.candleData.volume, null);
            chart.painter.rectangle(this.color, volume[0], volume[1], volume[2], volume[3], 0.2);
        }
    }
}