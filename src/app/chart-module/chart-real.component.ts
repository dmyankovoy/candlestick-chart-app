import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService } from './data.service';

import { ChartOptions } from './models'

@Component({
    selector: '[chart-real]',
    templateUrl: 'chart-real.component.html'
})
export class ChartRealComponent implements OnInit {
    chartData: any;
    chartOptions: ChartOptions;
    loadingFailed: boolean = false;
    
    constructor(private dataService: DataService) { 
        this.chartOptions = new ChartOptions();
        this.chartOptions.showVolumes = false;        
        this.chartOptions.showYAxis = true;
        this.chartOptions.axisYLabelFormatter = (val: number): string => {
            return (Math.round(val * 10000) / 10000).toString();
        };
        this.chartOptions.paddingLeft = 40;
    }

    getData() {
        this.dataService.getRealData().subscribe(data => {            
            this.loadingFailed = false;
            this.chartData = data;
        }, () => {
            this.loadingFailed = true;
        });
    }

    ngOnInit() {
        this.getData();
    }
}