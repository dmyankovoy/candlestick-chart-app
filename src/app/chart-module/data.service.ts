import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';

import * as data from './../../../info/data.json';

@Injectable()
export class DataService {
    constructor(private http: Http) {}

    getTrialData(): Observable<any> {
        return Observable.create((obs) => {
            obs.next(data);
        });
    }

    getRealData(): Observable<any> {
        const toSec = Math.ceil(new Date().getTime() / 1000);
        const fromSec = Math.ceil(toSec - 2 * 24 * 60 * 60);
        
        const api = `/externalApi/8d162a7b03df2592780c132bad60562f/${toSec}/7/7/18/history`;
        return this.http.get(api, { params: {
            from: fromSec,
            to: toSec,
            resolution: 15,
            symbol: 1
        }}).map(res => res.json());
    }
}