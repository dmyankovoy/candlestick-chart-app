import {
    Directive, HostListener, Input, ElementRef, NgZone,
    OnInit, OnChanges, SimpleChanges, SimpleChange
} from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Chart } from './models/chart';
import { ChartOptions } from './models';

@Directive({
    selector: '[chart]'
})
export class ChartDirective implements OnChanges {
    @Input('chart') chartData: any;
    @Input('options') options: any;

    chart: Chart;

    constructor(private el: ElementRef, private ngZone: NgZone) {
        const canvasEl = document.createElement('canvas');
        el.nativeElement.parentElement.replaceChild(canvasEl, el.nativeElement);

        const canvasOptions = new ChartOptions();
        this.chart = new Chart(canvasEl, canvasOptions);
    }

    @HostListener('window:resize', ['$event'])
    onResize(): void {
        this.ngZone.run(() => {
            this.chart.resize();
        });
    }    

    initChart(chartData): void {
        if (chartData === undefined) {
            return;
        }

        this.chart.setData(chartData);
        this.chart.render();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.options) {
            this.chart.setOptions(Object.assign(new ChartOptions(), changes.options.currentValue));
        }

        if (changes.chartData) {
            this.initChart(changes.chartData.currentValue);
        }
    }
}