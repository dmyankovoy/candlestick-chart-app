import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService } from './data.service';

@Component({
    selector: '[chart-trial]',
    templateUrl: 'chart-trial.component.html'
})
export class ChartTrialComponent implements OnInit {
    constructor(private dataService: DataService) { }

    chartData: any;

    ngOnInit() {        
        this.dataService.getTrialData().subscribe(data => {
            this.chartData = data;
        });
    }
}