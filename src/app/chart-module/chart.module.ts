import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ChartTrialComponent } from './chart-trial.component';
import { ChartRealComponent } from './chart-real.component';
import { ChartDirective } from './chart.directive';
import { DataService } from './data.service';

@NgModule({
    declarations: [
        ChartTrialComponent,
        ChartRealComponent,
        ChartDirective
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: 'chart-trial', component: ChartTrialComponent },
            { path: 'chart-real', component: ChartRealComponent }
        ])
    ],
    exports: [
        RouterModule,
        ChartRealComponent,
        ChartTrialComponent
    ],
    providers: [
        DataService
    ]
})
export class ChartModule { }