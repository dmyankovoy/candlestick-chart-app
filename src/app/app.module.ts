import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { ChartModule } from './chart-module/chart.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [    
    BrowserModule,
    HttpModule,
    RouterModule.forRoot([{ path: '', redirectTo: 'chart-trial', pathMatch: 'full' }], { useHash: true }),    
    ChartModule    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
